import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    static int total = 0;
    static String orderedItemList = "";
    static void order(String food, JTextPane textPane1, int price, JLabel totalPrice){
        int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to order " + food + "?", "Order Confirmation", JOptionPane.YES_NO_OPTION);

        if (confirmation == 0){
            total += price;
//            orderedItemList += food + "\n";
            orderedItemList += food + " " + String.valueOf(price) + "yen\n";
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "! It will be served as soon as possible.");
            textPane1.setText(orderedItemList);
            totalPrice.setText("Total " + String.valueOf(total) + " yen");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton udonButton;
    private JButton yakisobaButton;
    private JButton ramenButton;
    private JTextPane textPane1;
    private JPanel root;
    private JButton checkOutButton;
    private JLabel totalPrice;

    public FoodGUI() {

        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", textPane1, 100, totalPrice);
            }
        });
        tempuraButton.setIcon(new ImageIcon(
                this.getClass().getResource(".\\gazou\\tempura.jpg")
        ));
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage", textPane1, 200, totalPrice);
            }
        });
        karaageButton.setIcon(new ImageIcon(
                this.getClass().getResource(".\\gazou\\karaage.jpg")
        ));
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza", textPane1, 300, totalPrice);
            }
        });
        gyozaButton.setIcon(new ImageIcon(
                this.getClass().getResource(".\\gazou\\gyoza.jpg")
        ));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", textPane1, 400, totalPrice);
            }
        });
        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource(".\\gazou\\udon.png")
        ));
        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakisoba", textPane1, 500, totalPrice);
            }
        });
        yakisobaButton.setIcon(new ImageIcon(
                this.getClass().getResource(".\\gazou\\yakisoba.jpeg")
        ));
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", textPane1, 600, totalPrice);
            }
        });
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource(".\\gazou\\ramen.jpg")
        ));
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to checkout?", "Checkout Confirmation", JOptionPane.YES_NO_OPTION);
                if (confirmation == 0){
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is "+ total + " yen.");
                    total = 0;
                    orderedItemList = "";
                    totalPrice.setText("Total " + String.valueOf(total) + " yen");
                    textPane1.setText(orderedItemList);
                }
            }
        });
        totalPrice.setText("Total " + String.valueOf(total) + " yen");
    }
}
